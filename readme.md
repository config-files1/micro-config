# Micro Config

Configuration files for the Micro text editor

Copy the keybindings json file to the micro config directory
```
cp -v bindings.json ~/.config/micro/bindings.json
```

Copy the settings json file to the micro config directory
```
cp -v settings.json ~/.config/micro/settings.json
```

## Install Micro Plugins

Install lsp client plugin
```
micro -plugin install lsp
```

Install file manger plugin
```
micro -plugin install filemanager
```

## Install LSP packages

Install Go Language Server
```
sudo apt install golang-go gopls
```

Install Rust Language Server
```
rustup component add rls rust-analysis rust-src
```

Install Python dependencies for LSP
```
sudo apt install python3-pip virtualenv
```

Install Python Language Server
```
pip install python-lsp-server[rope,pyflakes,mccabe,pylsp-mypy] pylsp-mypy
```
